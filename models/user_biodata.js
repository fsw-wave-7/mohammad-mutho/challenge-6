'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User_biodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  User_biodata.init({
    full_name: DataTypes.STRING,
    age: DataTypes.STRING,
    sex: DataTypes.STRING,
    city: DataTypes.STRING,
    address: DataTypes.STRING,
    email: DataTypes.STRING,
    user_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'User_biodata',
  });
  return User_biodata;
};