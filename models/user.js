'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.hasOne(models.User_biodata, {
				foreignKey: "user_id",
				as: "biodata_id",
				onDelete: "CASCADE",
				onUpdate: "CASCADE",
			});
      // one-to-many
      User.hasMany(models.User_history, {
				foreignKey: "user_id",
				as: "history_id",
				onDelete: "CASCADE",
				onUpdate: "CASCADE",
			});
    }
  };
  User.init({
    name: DataTypes.STRING,
    username: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};