const bodyParser = require('body-parser')
const { Router } = require('express')

const ResponseMiddleware = require('../middlewares/ResponseMiddleware.js')

const UserController = require('../controllers/api/UserController.js')

const api = Router()
const userController = new UserController();

api.use(bodyParser.json())
api.use(ResponseMiddleware())

api.get('/user', userController.getUser)
api.get('/user/:id', userController.getDetailUser)
api.post('/user', userController.insertUser)
api.put('/user/:id', userController.updateUser)
api.delete('/user/:id', userController.deleteUser)

module.exports = api