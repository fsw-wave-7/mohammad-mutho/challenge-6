const { Router } = require('express')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const AuthMiddleware = require('../middlewares/AuthMiddleware')

const AuthController = require('../controllers/web/AuthController')
const HomeController = require('../controllers/web/HomeController')

const web = Router()

const authcontroller = new AuthController
const homeContreoller =  new HomeController

web.use(bodyParser.json())
web.use(bodyParser.urlencoded({ extended: true}))
web.use(cookieParser())

web.get('/login', authcontroller.login)
web.post('/login', authcontroller.doLogin)
web.get('/logout', authcontroller.logout)

// web.use(AuthMiddleware())

web.get('/', homeContreoller.index)
web.get('/show-histories', homeContreoller.showHistory)
web.post('/save-user', homeContreoller.saveUser)
web.get('/add', homeContreoller.add)
web.get('/edit/:id', homeContreoller.editUser)
web.post('/edit/:id', homeContreoller.updateUser)
web.get('/delete/:id', homeContreoller.delete)

module.exports = web