const bcrypt = require('bcrypt')
const { User } = require('../../models')

class AuthController{

  login = (req, res) => {
    res.render("../views/login")
  }

  doLogin = async (req, res) => {
    const body = req.body;

    if (!(body.username && body.password)){
      return res.status(400).send({
        error: "Data not formatted Properly"
      });
    }

    User.findOne({
      where: { username : body.username }
    })
    .then(user => {
      bcrypt.compare(body.password, user.password, (err, data) => {
        if (err) throw err

        if (data) {
          res.cookie('loginData', JSON.stringify(user))
          res.redirect('/')
        } else {
          return res.status(401).json({
            msg: "Invalid credential"
          })
        }
      });
    })
    .catch(err => {
      return res.status(401).json({
        msg: "Invalid Credential"
      })
    })
  }

  logout = (req, res) => {
    res.clearCookie('loginData')
    res.redirect('/')
  }
}
module.exports = AuthController