const { User, User_biodata, User_history } = require('../../models')
const bcrypt = require("bcrypt")

class HomeController{

  index = (req, res) => {
    User.findAll({
      include: [
        {
          model: User_biodata,
          as: "biodata_id",
        }, {
          model: User_history,
          as: "history_id",
        },
      ],
    })
    .then(user => {
      res.render("../views/index", {
        content: './pages/userList',
        user: user
      })
    })
  }

  add = (req, res) => {
    res.render("../views/index", {
      content: './pages/addList'
    })
  }

  showHistory = (req, res) => {
    User_history.findAll({
      include: [
        {
          model: User,
          as: "user",
          attributes: ['name', 'username']
        }
      ],
    })
    .then(user => {
      res.render("../views/index"), {
        content: './pages/historyList',
        user: user
      }
    })
  }

  saveUser = async (req, res) => {
    const salt = await bcrypt.genSalt(10)

    User.create({
      name: req.body.name,
      username : req.body.username,
      password: await bcrypt.hash(req.body.password, salt),
      biodata_id: {
        full_name: req.body.fullname,
        city: req.body.city,
        age: req.body.age,
        sex: req.body.gender,
        address: req.body.address,
        email: req.body.email,
      },
      history_id: {
        score: Math.floor(Math.random() * 60 + 1),
        play_time: `${Math.floor(Math.random() * 100)} minutes`,
      }
    },{
      include: [
        {
          model: User_biodata,
          as: "biodata_id",
        }, {
          model: User_history,
          as: "history_id",
        },
      ],
    })
    .then(user => {
      res.redirect('/')
    }).catch(err => {
      console.log(err)
      res.status(422).json("Can't Created User")
    })
  }

  editUser = (req, res) => {
    User.findOne({
      where: {
        id: req.params.id
      },
      include: [
        {
          model: User_biodata,
          as: "biodata_id",
        }, {
          model: User_history,
          as: "history_id",
        }
      ]
    })
    .then(user => {
      res.render("../views/index", {
        content : './pages/editList',
        user: user
      })
    })
  }

  updateUser = (req, res) => {
    User.update({
      name: req.body.name,
      username : req.body.username,
      password: req.body.password
    }, {
      where: { id: req.params.id }
    });
    User_biodata.update({
      full_name: req.body.fullname,
      city: req.body.city,
      age: req.body.age,
      sex: req.body.gender,
      address: req.body.address,
      email: req.body.email
    }, {
      where: { user_id : req.params.id }
    })
    .then(user => {
      res.redirect('/')
    }).catch(err => {
      res.status(422).json("Can't update user")
    })
  }

  delete = (req, res) => {
    User.destroy({
      where: { id: req.params.id }
    });
    User_biodata.destroy({
      where: { user_id: req.params.id }
    });
    User_history.destroy({
      where: { user_id: req.params.id }
    })
    .then(() => {
      res.redirect('/')
    })
  }
}

module.exports = HomeController