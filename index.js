const { Console } = require('console')
const express = require('express')
const morgan = require('morgan')
const path = require('path')
const { join } = require('path')
const api = require('./routes/api.js')
const web = require('./routes/web.js')

const app = express()
const port = 3000

//Load Static File
app.use(express.static(__dirname + '/public'))

//Set view engine
app.set('view engine', 'ejs')

//Third Party Middleware for Logging
app.use(morgan('dev'))

//Load routes
app.use('/api', api)
app.use('/', web)

//Internal Server Error Handle Middleware
app.use(function(err, req, res, next){
  res.status(500).json({
    status: 'fail',
    errors: err.message
  })
})

//404 Handler Middleware
app.use(function (req, res, next) { 
  res.render(join(__dirname, './views/404'))
})

app.listen(port, () => {
  console.log("Server berhasil Dijalankan")
})